package com.sunbeam;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.sunbeam.entities.User;
import com.sunbeam.services.UserServiceImpl;

@SpringBootApplication
public class Demo16AssignmentApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(Demo16AssignmentApplication.class, args);
	}
	@Autowired
	private UserServiceImpl userService;
	@Override
	public void run(String... args) throws Exception {
		
		User b=userService.findUserById(1);
		System.out.println(b);
	}

}
