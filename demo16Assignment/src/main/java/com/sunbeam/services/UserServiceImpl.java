package com.sunbeam.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sunbeam.daos.UserDao;

import com.sunbeam.entities.User;

@Transactional
@Service
public class UserServiceImpl {
@Autowired
	private UserDao userDao;

public User findUserById(int id) {
	Optional<User>b=userDao.findById(id);
	return b.orElse(null);
}
}
